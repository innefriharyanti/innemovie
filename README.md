
## MySFtube - untuk mobile test di smartfren

- React native 0.60+
- All business logic in redux
- Typescript support
- Eslint integration
- Performance optimization


## Memulai aplikasi

1. [Set up React Native.]
2. `cd` into this project directory
3. `pod install` in `ios` directory
4. `npm install` or `yarn install`
5. Run `react-native run-android` or `react-native run-ios`

## API

(https://developers.themoviedb.org/3)

# Apabila gagal menjalankan aplikasi
 1. Clear watchman watches: watchman watch-del-all
 2. Delete node_modules: rm -rf node_modules and run yarn install
 3. Reset Metro's cache: yarn start --reset-cache
 4. Remove the cache: rm -rf /tmp/metro-*
